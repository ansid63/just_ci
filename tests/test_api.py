import requests
import pytest

from api_data import RickAndMortyApiData


class TestRickAndMortyApi:
    @pytest.mark.smoke_service_a
    def test_main_endpoint(self, url):
        response = requests.get(url)
        assert response.json()["characters"] == RickAndMortyApiData.characters_api_url, "Некорректный ответ"

    @pytest.mark.smoke_service_b
    def test_character_endpoint(self, url):
        response = requests.get(url + "/character")
        assert response.json()["info"]["count"] == RickAndMortyApiData.number_of_characters, "Некорректный ответ"
