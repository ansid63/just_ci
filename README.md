## Организация автоматического запуска автотестов с использованием Downstream pipelines в GitLab CI.

Представим, что бизнес пришёл к нам с задачей, настроить автоматический запуск автотестов при деплое сервиса на стенды. При этом микросервисов несколько, они должны триггерить разные группы автотестов, а также имеют разные точки входа, т.е. базовые URL.

![Services connection](article_artifacts/services.png)

### Пайплайн сборки сервисов.

Допустим .gitlab-ci.yml в сборке микросервиса выглядит так:

```yaml
stages:
  - build
  - deploy
  - test

build job:
  stage: build
  script:
    - echo "Build service"

deploy job:
  stage: deploy
  script:
    - echo "Deploy service"
  when: on_success
```

Сервис при сборке хранит в переменных среды необходимый URL, который далее станет точкой входа для автотестов (например https://app.testing.com/ или https://app.staging.com/). Если этого URL в переменных нет, но Вы используете например Kubernetes, в настройках этого пайплайна  в UI GitLab путь Setting -> CI/CD -> Variables, Вы можете добавить разные переменные для разных окружений, например PARENT_URL для разных Environment scope. 

![Add_vars_gitlab](article_artifacts/add_vars.jpg)

Мы пойдем по пути использования одного фреймворка с автотестами для этих микросервисов. Мы хотим, чтобы фреймворк жил отдельным пайплайном и мог автономно запускаться, без привязки к микросервису.

Для чего нужен автономный запуск. Когда мы написали новый тест-сьюит и его запуск успешно прошел локально, необходимо проверить как его прогон пройдет в CI/CD, т.к. там автотесты могут упасть.
Связано это может быть с тем, что под капотом Selenoid автотест может работать иначе, или скорость обработки данных БД чрезмерно высока и в неё не успевают прилетать сообщения из другого сервиса.

Чтобы автотесты запускались автоматом при деплое сервиса и при это мы имели необходимую автономность, мы будем использовать [Downstream pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html).

Добавляем в .gitlab-ci.yml сборки микросервиса триггер для запуска автотестов:

```yaml
testing_job: 
    stage: test 
    variables:
        PARENT_MARK: "smoke_service_a"  # Здесь Вы можете указать метку, которую хотите передавать в автозапуск тестов 
        # Для второго микросервиса здесь будет соответственно PARENT_MARK: "smoke_service_b"
        PARENT_URL: "https://rickandmortyapi.com/api" # Здесь я имитирую передачу сервисом необходимого URL
    trigger: ansid63/just_ci
    rules:
      - if: $CI_COMMIT_BRANCH == "main"
```

В trigger мы передаем путь к нашему проекту с автотестами в GitLab CI.
Поле rules, в данном случае, устанавливает условие, что автотесты необходимо триггерить при мерже в ветку main.

### Пайплайн проекта с автотестами.

Переходим к проекту с автотестами. Я сделал небольшой проект с API тестами для проверки работы связки пайплайнов:

![Autotest project](article_artifacts/autotest_project.PNG)

В проекте должен быть подключен [GitLab Runner](https://docs.gitlab.com/runner/install/windows.html), если Вы запускаете проект на gitlab.com возможно Вам будет достаточно Runner предлагаемого сервисом GitLab.

В conftest.py я выдергиваю базовый URL для запуска автотестов:

```python
from pytest import fixture


def pytest_addoption(parser):
    parser.addoption(
        "--url",
        action="store",
        default="https://rickandmortyapi.com/api")


@fixture()
def url(request):
    return request.config.getoption("--url")
```

Dockerfile собирает контейнер с автотестами и зависимостями, в автотестах используются библиотеки pytest и requests.
```docker
FROM python:3.9.13-slim

COPY . .
RUN pip3 install -r requirements.txt --no-cache-dir 
```

Папка tests содержит один файл с автотестами.

Давайте разберем пайплайн в .gitlab-ci.yml проекта с автотестами:
```yaml
stages:
  - build  # Этап сборки контейнера с автотестами и зависимостями 
  - test   # Этап запуска автотестов

variables:
  TEST_MARK:
    value: "smoke_service_a"  # Дефолтная mark для pytest
  TEST_PATH:
    value: "tests"  
    # Т.к. мы прокидываем --url, pytest очень просит указать путь к местоположению тестов
  TEST_URL:
    value: "https://rickandmortyapi.com/api"  # Дефолтный URL для pytest
  AQA_GIT_TAG: v0.1.0  # Тэг для контейнера с автотестами
  AQA_IMAGE: "${CI_REGISTRY_IMAGE}/autotests:${AQA_GIT_TAG}" # Путь к контейнеру с автотестами


.docker-registry: &docker-registry
  - echo $CI_REGISTRY_PASSWORD | docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY 
# Шаблон для авторизации в СI registry 

build_autotest_image:  
  # Данный этап с условием only позволяет нам собирать контейнер только при git push
  # и избежать постоянной пересборки контейнера при запуске автотестов.
  stage: build
  image: gitlab/dind
  services:
    - docker:dind
  before_script:
    - *docker-registry  # Авторизация в СI registry
  script:
    - docker build -t ${AQA_IMAGE} .  # Собираем контейнер с автотестами и зависимостями
    - docker push ${AQA_IMAGE}        # Пушим контейнер в СI registry
  only:
    refs:
      - pushes  # Сборка контейнера происходит только при git push в проекте с автотестами


test job 1:  # Запуск автотестов при триггере от сборки микросервиса
    stage: test
    image: "${AQA_IMAGE}"
    script:
      - pytest -m $PARENT_MARK $TEST_PATH --url $PARENT_URL  # Запускаем автотесты
    rules:
      - if: $CI_PIPELINE_SOURCE == "pipeline"  
      # Согласно данному правилу, этот процесс запускается в случае, если пайплайн триггериться


test job 2:  # Запуск автотестов при ручном запуске
    stage: test
    image: "${AQA_IMAGE}"
    script:
      - pytest -m $TEST_MARK $TEST_PATH --url $TEST_URL  # Запускаем автотесты
    rules:
      - if: $TEST_FROM == "handheld"  
      # Запуск происходит когда в Run Pipeline передаем TEST_FROM == "handheld"
```
### Запуск автотестов.

При запуске сборки и деплоя микросервиса, у нас автоматически стартуют автотесты:

![Service_pipeline](article_artifacts/service_pipeline.PNG)

Если автотесты пройдут с ошибками, билд будет обозначен как failed, и будет подсвечиваться красным, возможно это то, чего ожидал бизнес от этой схемы запуска.
В случае если Вы не хотите "фейлить" деплой, а лишь подсветить, что прогон автотестов прошел с падениями, следует добавить allow_failure: true в джобу stage: test. 

Если мы хотим запустить автотесты для дефолтных значений в нашем проекте с автотестами, мы переходим во вкладку CI/CD GitLab, нажимаем Run pipeline.
В появившемся окне необходимо выбрать branch, а в Variables добавить TEST_FROM со значением handheld, нажать кнопку Run pipeline.
Как результат, Вы получите прогон автотестов с меткой smoke_service_a, и дефолтным URL "https://rickandmortyapi.com/api".

Если Вам нужна более гибкая настройка и Вам нужен запуск ветки develop, метки smoke_service_b, файла с автотестами по пути tests/test_api.py::TestRickAndMortyApi и с определенным входным URL это можно сделать так:
![Autotest_pipeline](article_artifacts/autotest_pipeline.PNG)

Нажимаете Run pipeline, получаете необходимый Вам запуск автотестов. Плюс этого подхода, он даёт Вам высокую гибкость относительно вариантов запуска.

![Autotest_done](article_artifacts/autotest_done.PNG)

К преимуществам Downstream_pipelines можно отнести возможность взаимодействия нескольких проектов, а также возможность передачи необходимых переменных из одного проекта в другой, на основании чего Вы можете строить необходимые Вам зависимости.
Если Вы используете один стенд для запуска автотестов и прогон автотестов необходим при сборке одного сервиса, можно также использовать Downstream_pipelines для автономности проекта с автотестами.
