from pytest import fixture


def pytest_addoption(parser):
    parser.addoption(
        "--url",
        action="store",
        default="https://rickandmortyapi.com/api")


@fixture()
def url(request):
    return request.config.getoption("--url")
