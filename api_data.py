class RickAndMortyApiData:
    base_url = "https://rickandmortyapi.com/api"
    characters_api_url = "https://rickandmortyapi.com/api/character"
    number_of_characters = 826
